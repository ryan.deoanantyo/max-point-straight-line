package com.maxpoint.max_point_ryan;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
	private InputPoint[] inputPoints = null;
	private int numberOfPoints = 0;
	
	public void getPoints() {
		Scanner sc = new Scanner(System.in);
    	System.out.println("How many input points do you want?");
    	numberOfPoints = sc.nextInt();
    	
    	inputPoints = new InputPoint[numberOfPoints];
    	for(int index=0; index<numberOfPoints; index++) {
    		System.out.println("index " + (index+1) + " (x,y) = ");
    		String splitOfPointByComma[] = sc.next().split(",");
    		int xTemp = Integer.parseInt(splitOfPointByComma[0]);
    		int yTemp = Integer.parseInt(splitOfPointByComma[1]);
    		inputPoints[index] = new InputPoint();
    		inputPoints[index].setX(xTemp);
    		inputPoints[index].setY(yTemp);
    	}
    }
	public void printPoints() {
    	for(int index=0; index<numberOfPoints; index++) {
    		System.out.println("(" + inputPoints[index].getX() + ", " + inputPoints[index].getY() + ")");
    	}	
    }
	public void printMaxPoint(ApplicationContext context) {
		Solution solution = (Solution) context.getBean("solutionBean");
		System.out.println("maximum point = " + solution.findSolution(inputPoints));
	}
	
	public static void main( String[] args )
    {
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
		App app = (App) context.getBean("appBean");
    	app.getPoints();
    	//app.printPoints();
    	app.printMaxPoint(context);
    }
}
