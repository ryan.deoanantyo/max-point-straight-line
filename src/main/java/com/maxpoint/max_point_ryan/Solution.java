package com.maxpoint.max_point_ryan;

public class Solution {
	private InputPoint[] points;
	public int findSolution(InputPoint[] points) {
        // logic to be added here
		int maxPoint = 0;
		for(int index=0; index<points.length; index++) {
			if (points[index].getX()>maxPoint) {
				maxPoint = points[index].getX();
			}
		}
		return maxPoint;
    }
}